#!/usr/bin/env -S rsing python3

from argparse import ArgumentParser
from rucio.client.replicaclient import ReplicaClient
from rucio.client.ruleclient import RuleClient
from rucio.common.utils import adler32
from rucio.rse import get_rse_client
from os.path import basename
from os import stat

activity = "DAC21"


def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument(
        "-i",
        "--input",
        required=True,
        help="Input path to register (file name is will be taken from here).",
    )
    parser.add_argument(
        "-s", "--scope", required=True, help="Rucio scope to register the data to."
    )
    parser.add_argument("-r", "--rse", required=True, help="RSE with the data.")
    parser.add_argument(
        "-ll",
        "--lifetimel",
        type=int,
        required=True,
        help="Total lifetime of the file.",
    )
    parser.add_argument(
        "-ls",
        "--lifetimes",
        type=int,
        required=True,
        help="Lifetime of the file on staging after upload.",
    )
    parser.add_argument(
        "-ql",
        "--qosl",
        type=str,
        required=True,
        help="QoS level for long-term storage.",
    )
    parser.add_argument(
        "-qs", "--qoss", type=str, required=True, help="QoS level for staging area."
    )
    parser.add_argument("-p", "--prot", default="davs", help="Protocol, default davs.")
    parser.add_argument(
        "-f",
        "--file",
        help="Original file (for checksum and size, if not specified will assume this is the same as input)",
    )
    args = parser.parse_args()
    return args


def get_prot_URL(rse, scheme):
    rse_client = get_rse_client(rse)
    allprots = rse_client["protocols"]
    for protocol in allprots:
        if protocol["scheme"] == scheme:
            return f"{scheme}://{protocol['hostname']}:{protocol['port']}{protocol['prefix']}"
    raise IndexError(f"RSE {rse} has no entry for protocol {scheme}")


def get_file_details(filepath):
    adler32sum = adler32(filepath)
    filesize = stat(filepath).st_size
    return filesize, adler32sum


def register_data(rse, scope, protocol, inputpath, lifetime, filepath=None):
    data_url_root = get_prot_URL(rse=args.rse, scheme=protocol)
    data_url = data_url_root + "/" + scope + "/" + args.input.lstrip("/")
    data_filename = basename(inputpath)
    if filepath == None:
        filepath = inputpath
    file_size, adler32_checksum = get_file_details(filepath)
    rep_cli = ReplicaClient()
    rep_cli.add_replica(
        rse=rse,
        scope=scope,
        name=data_filename,
        bytes=file_size,
        adler32=adler32_checksum,
        pfn=data_url,
    )

    rul_cli = RuleClient()
    return rul_cli.add_replication_rule(
        dids=[{"scope": scope, "name": data_filename}],
        copies=1,
        rse_expression=rse,
        lifetime=lifetime,
        purge_replicas=True,
        activity=activity,
    )


def apply_QoSlevel(scope, filepath, qos, lifetime):
    filename = basename(filepath)
    rul_cli = RuleClient()
    return rul_cli.add_replication_rule(
        dids=[{"scope": scope, "name": filename}],
        copies=1,
        rse_expression=f"QOS={qos}",
        lifetime=lifetime,
        activity=activity,
        purge_replicas=True,
    )


def del_rule(ruleid):
    rul_cli = RuleClient()
    rul_cli.delete_replication_rule(
        ruleid, purge_replicas=True
    )  ## probably we are purging twice...
    rul_cli.update_replication_rule(ruleid,{"lifetime":1})


if __name__ == "__main__":
    args = parse_arguments()
    if args.lifetimel < args.lifetimes:
        raise ValueError("Long-term lifetime needs to be longer than staging lifetime.")

    prot_ruleid = register_data(
        rse=args.rse,
        scope=args.scope,
        inputpath=args.input,
        protocol=args.prot,
        filepath=args.file,
        lifetime=args.lifetimel,
    )
    print(f"Registration succeeded, temporary protection rule: {prot_ruleid}")

    qos_l_rule = apply_QoSlevel(
        scope=args.scope, filepath=args.input, qos=args.qosl, lifetime=args.lifetimel
    )
    qos_s_rule = apply_QoSlevel(
        scope=args.scope, filepath=args.input, qos=args.qoss, lifetime=args.lifetimes
    )
    del_rule(prot_ruleid[0])

    print(f"{qos_s_rule[0]}: keep data for {args.lifetimes} seconds on QOS {args.qoss}")
    print(f"{qos_l_rule[0]}: keep data for {args.lifetimel} seconds on QOS {args.qosl}")
