#!/usr/bin/env bash

#eval `oidc-agent`
#oidc-add ESCAPE

function refreshtoken {
   while true
   do
    oidc-token ESCAPE --aud=rucio --scope "openid profile offline_access" | tr -d '\n' > /tmp/grange/.rucio_grange/auth_token_for_account_grange
    sleep 60
   done
}

my_dir=${PWD}
uploaddir=$(basename $1)
cd `dirname $1`

refreshtoken &
tokpid=$!

rclone copy -L ${uploaddir}/ DESY-WEBDAV:${uploaddir}/ -P

function do_reg {
    echo copyting ${1}...
    ${my_dir}/register_uploaded.py -i $uploaddir/${1} -s LOFAR_ASTRON_GRANGE -r DESY-DCACHE-NDR -ll 36000 -ql SAFE -ls 10800 -qs CHEAP-ANALYSIS  -p davs
    echo ${1} done!
}

export uploaddir
export -f do_reg
export my_dir

parallel -j 5 do_reg {1} ::: `ls ${uploaddir}`

kill $tokpid
wait $tokpid 2>/dev/null


#for filename in `ls $uploaddir`
#do
#echo $filename
#rclone copy $uploaddir/$filename DESY-WEBDAV:$uploaddir
#${my_dir}/register_uploaded.py -i $uploaddir/$filename -s LOFAR_ASTRON_GRANGE -r DESY-DCACHE-NDR -ll 3000 -ql OPPORTUNISTIC -ls 500 -qs CHEAP-ANALYSIS  -p davs
#done

